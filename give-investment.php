<?php
namespace GiveInvestment;

use GiveInvestment\Addon\Activation;
use GiveInvestment\Addon\Environment;
use GiveInvestment\Investment\AddonServiceProvider;

/**
 * Plugin Name:         Give - Investment
 * Plugin URI:          https://givewp.com/addons/BOILERPLATE/
 * Description:         Adds investment metrics and tracking to the dashboard.
 * Version:             1.0.0
 * Requires at least:   4.9
 * Requires PHP:        5.6
 * Author:              Give for Good
 * Author URI:          https://giveforgood.world/
 * Text Domain:         give-investment
 * Domain Path:         /languages
 */
defined('ABSPATH') or exit;

// Add-on name
define('GIVE_INVESTMENT_NAME', 'Give - Investment');

// Versions
define('GIVE_INVESTMENT_VERSION', '1.0.0');
define('GIVE_INVESTMENT_MIN_GIVE_VERSION', '2.8.0');

// Add-on paths
define('GIVE_INVESTMENT_FILE', __FILE__);
define('GIVE_INVESTMENT_DIR', plugin_dir_path(GIVE_INVESTMENT_FILE));
define('GIVE_INVESTMENT_URL', plugin_dir_url(GIVE_INVESTMENT_FILE));
define('GIVE_INVESTMENT_BASENAME', plugin_basename(GIVE_INVESTMENT_FILE));

require 'vendor/autoload.php';

// Activate add-on hook.
register_activation_hook(GIVE_INVESTMENT_FILE, [Activation::class, 'activateAddon']);

// Deactivate add-on hook.
register_deactivation_hook(GIVE_INVESTMENT_FILE, [Activation::class, 'deactivateAddon']);

// Uninstall add-on hook.
register_uninstall_hook(GIVE_INVESTMENT_FILE, [Activation::class, 'uninstallAddon']);

// Register the add-on service provider with the GiveWP core.
add_action(
    'before_give_init',
    function () {
        // Check Give min required version.
        if (Environment::giveMinRequiredVersionCheck()) {
            give()->registerServiceProvider(AddonServiceProvider::class);
        }
    }
);

// Check to make sure GiveWP core is installed and compatible with this add-on.
add_action('admin_init', [Environment::class, 'checkEnvironment']);
