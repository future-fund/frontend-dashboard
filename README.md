# Give for Good - GiveWP Add-On: Investment Dashboard

## Introduction

Expanding the frontend Donor Dashboard to include data about A) how much donations are currently worth and B) how much money has been transferred to charity because of the donations.

This repo is based on the [GiveWP Add-On boilerplate repository](https://github.com/impress-org/givewp-addon-boilerplate).

## Development

### Getting Set Up

1. Clone this repository locally
2. Run `composer install` from the CLI
3. Run `npm install` from the CLI

### Asset Compilation

To compile your CSS & JS assets, run one of the following:
- `npm run dev` — Compiles all assets for development one time
- `npm run watch` — Compiles all assets for development one time and then watches for changes, supporting [BrowserSync](https://laravel-mix.com/docs/5.0/browsersync)
- `npm run hot` — Compiles all assets for development one time and then watches for [hot replacement](https://laravel-mix.com/docs/5.0/hot-module-replacement)
- `npm run dev` — Compiles all assets for production one time
