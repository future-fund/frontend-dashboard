<?php

namespace GiveInvestment\Investment;

/**
 * Helper class responsible for loading add-on assets.
 *
 * @package     GiveInvestment\Addon
 * @copyright   Copyright (c) 2020, GiveWP
 */
class Assets
{

    /**
     * Load add-on backend assets.
     *
     * @since 1.0.0
     * @return void
     */
    public static function loadBackendAssets()
    {
        wp_enqueue_style(
            'give-investment-style-backend',
            GIVE_INVESTMENT_URL . 'public/css/give-investment-admin.css',
            [],
            GIVE_INVESTMENT_VERSION
        );

        wp_enqueue_script(
            'give-investment-script-backend',
            GIVE_INVESTMENT_URL . 'public/js/give-investment-admin.js',
            [],
            GIVE_INVESTMENT_VERSION,
            true
        );
    }

    /**
     * Load add-on front-end assets.
     *
     * @since 1.0.0
     * @return void
     */
    public static function loadFrontendAssets()
    {
        wp_enqueue_style(
            'give-investment-style-frontend',
            GIVE_INVESTMENT_URL . 'public/css/give-investment.css',
            [],
            GIVE_INVESTMENT_VERSION
        );

        wp_enqueue_script(
            'give-investment-script-frontend',
            GIVE_INVESTMENT_URL . 'public/js/give-investment.js',
            [],
            GIVE_INVESTMENT_VERSION,
            true
        );
    }
}
