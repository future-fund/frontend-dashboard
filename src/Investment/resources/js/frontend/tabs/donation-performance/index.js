import Content from './content';

export const registerDonationPerformanceTab = () =>
  window.giveDonorDashboard.utils.registerTab({
    label: 'Donation Performance', // TODO: i18n
    icon: 'chart-line',
    slug: 'donation-performance',
    content: Content,
  });