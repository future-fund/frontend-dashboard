import Content from './content';

export const registerDonationStatisticsTab = () =>
  window.giveDonorDashboard.utils.registerTab({
    label: 'Donation Statistics', // TODO: i18n
    icon: 'chart-pie',
    slug: 'donation-statistics',
    content: Content,
  });