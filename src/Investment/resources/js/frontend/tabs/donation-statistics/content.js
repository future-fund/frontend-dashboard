const Content = () => {
  /* 
   * The iframe below requires that the user has created a page called
   * 'donation-statistics' in the frontend, that has NO headers,
   * footers, etc., just the relevant wpDataTable.
   *
   * Follow these instructions to create a page that fits well into
   * the iframe:
   * 
   * 1. Create a new page with elementor.
   * 2. Set the theme to `Elementor Canvas`, this will remove global
   *    headers, footers, etc.
   * 3. Add a shortcode element and paste the wpDataTable shortcode in.
   * 4. Set the section to "Full Width".
   * 5. Set the element to "Full Width".
   * 6. Set the background of the page to white to match.
   * 7. Add the following custom CSS to disable the admin bar and fix
   *    top alignment:
   * 
   * ```css
   * #wpadminbar {
   *     display: none;
   * }
   * 
   * body.page-template {
   *     margin-top: -56px;
   * }
   * 
   * .elementor-column-gap-default > .elementor-column > .elementor-element-populated {
   *     padding: 0px;
   * }
   * ```
   * 
   * For the wpDataTable:
   * 
   * 1. Disable the table title.
   * 2. Select the base skin `Material`.
   * 3. Add the following custom CSS:
   * 
   * ```css
   * table.wpDataTable.wpDataTableID-<YOUR-WPDT-ID> * {
   *     font-family: 'Montserrat', sans-serif;
   *     font-weight: 500;
   * }
   * 
   * table.wpDataTable.wpDataTableID-<YOUR-WPDT-ID> th {
   *     font-size: 12px;
   *     text-transform: uppercase;
   * }
   * 
   * table.wpDataTable.wpDataTableID-<YOUR-WPDT-ID> td {
   *     font-size: 14px;
   * }
   * ```
   */
  return window.React.createElement(
    'iframe', {
      src: 'donation-statistics',
      style: {
        'width': '100%',
        'min-height': '800px',
        'height': '100%;',
        'border': 'none',
      },
    }
  );
};

export default Content;
