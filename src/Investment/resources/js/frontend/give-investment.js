import {registerDonationPerformanceTab} from './tabs/donation-performance/index.js';
import {registerDonationStatisticsTab} from './tabs/donation-statistics/index.js';
import {makeReplacementReducer} from './reducer/index.js';

window.addEventListener('load', (_event) => {
  if (!window.giveDonorDashboardData)
    return;

  registerDonationPerformanceTab();
  registerDonationStatisticsTab();

  /* Replace the GiveWP redux reducer with a custom one extended with
    the ability to reorder the tabs to our liking. */
  const store = window.giveDonorDashboard.store;
  const reducer = makeReplacementReducer();
  store.replaceReducer(reducer);

  /* Dispatch custom action to reorder the tabs. */
  store.dispatch({
    type: 'REORDER_TABS',
    payload: {
      ordering: [
        'dashboard',
        'donation-performance',
        'donation-statistics',
        'donation-history',
        'recurring-donations',
        'edit-profile',
      ],
    },
  });
});