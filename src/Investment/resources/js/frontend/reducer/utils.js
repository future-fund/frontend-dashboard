export const getWindowData = (value) => {
  return window.giveDonorDashboardData[value];
};

export const getQueryParam = (param) => {
  const urlParams = new URLSearchParams(window.location.search);
  return urlParams.get(param);
};