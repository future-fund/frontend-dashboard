<?php
defined('ABSPATH') or exit; ?>

<div class="notice notice-error">
    <p>
        <strong><?php
            _e('Activation Error:', 'give-investment'); ?></strong>
        <?php
        _e('You must have the', 'give-investment'); ?> <a href="https://givewp.com" target="_blank">GiveWP</a>
        <?php
        printf(
            __('plugin installed and activated for the %s add-on to activate', 'give-investment'),
            GIVE_INVESTMENT_NAME
        ); ?>
    </p>
</div>
