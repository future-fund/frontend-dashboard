<?php
defined('ABSPATH') or exit; ?>

<strong>
    <?php
    _e('Activation Error:', 'give-investment'); ?>
</strong>
<?php
_e('You must have', 'give-investment'); ?> <a href="https://givewp.com" target="_blank">GiveWP</a>
<?php
_e('version', 'give-investment'); ?> <?php
echo GIVE_VERSION; ?>+
<?php
printf(esc_html__('for the %1$s add-on to activate', 'give-investment'), GIVE_INVESTMENT_NAME); ?>.

