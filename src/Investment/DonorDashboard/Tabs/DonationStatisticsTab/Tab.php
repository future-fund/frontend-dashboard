<?php

namespace GiveInvestment\Investment\DonorDashboard\Tabs\DonationStatisticsTab;

use Give\DonorDashboards\Tabs\Contracts\Tab as TabAbstract;

class Tab extends TabAbstract {

    public static function id()
    {
        return 'donation-statistics';
    }

    public function routes()
    {
        return [
            Route::class,
        ];
    }

}

?>