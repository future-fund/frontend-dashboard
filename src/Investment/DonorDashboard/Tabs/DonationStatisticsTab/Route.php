<?php

namespace GiveInvestment\Investment\DonorDashboard\Tabs\DonationStatisticsTab;

use Give\DonorDashboards\Repositories\Donations as DonationsRepository;
use Give\DonorDashboards\Tabs\Contracts\Route as RouteAbstract;

use WP_REST_Request;
use WP_REST_Response;

class Route extends RouteAbstract {

    public function endpoint() {
        return 'donation-statistics';
    }

    public function args() {
        return [];
    }

    public function handleRequest(WP_REST_Request $request)
    {
        return new WP_REST_Response([
            'status' => 200,
            'response' => 'success',
            'body_response' => [],
        ]);
    }

}

?>