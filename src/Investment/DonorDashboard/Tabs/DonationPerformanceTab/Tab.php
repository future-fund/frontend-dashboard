<?php

namespace GiveInvestment\Investment\DonorDashboard\Tabs\DonationPerformanceTab;

use Give\DonorDashboards\Tabs\Contracts\Tab as TabAbstract;

class Tab extends TabAbstract {

    public static function id()
    {
        return 'donation-performance';
    }

    public function routes()
    {
        return [
            Route::class,
        ];
    }

}

?>