<?php

namespace GiveInvestment\Investment;

use Give_Settings_Page;

/**
 * Example code to show how to add setting page to give settings.
 *
 * @package     GiveInvestment\Addon
 * @subpackage  Classes/Give_BP_Admin_Settings
 * @copyright   Copyright (c) 2020, GiveWP
 */
class SettingsPage extends Give_Settings_Page
{

    /**
     * Settings constructor.
     *
     * @since 1.0.0
     */
    public function __construct()
    {
        $this->id = 'give-investment';
        $this->label = esc_html__('Donation Investment Information', 'give-investment');
        $this->default_tab = 'general';

        parent::__construct();
    }

    /**
     * Add setting sections.
     *
     * @since 1.0.0
     * @return array
     */
    public function get_sections()
    {
        return [
            'general' => __('General', 'give-investment'),
        ];
    }

    /**
     * Get setting.
     *
     * @since 1.0.0
     * @return array
     */
    public function get_settings()
    {
        $current_section = give_get_current_setting_section();

        switch($current_section) {
            case "general":
                return [];
        }
    }
}
