<?php

namespace GiveInvestment\Addon;

use Give_Addon_Activation_Banner;

/**
 * Helper class responsible for showing add-on Activation Banner.
 *
 * @package     GiveInvestment\Addon\Helpers
 * @copyright   Copyright (c) 2020, GiveWP
 */
class ActivationBanner
{

    /**
     * Show activation banner
     *
     * @since 1.0.0
     * @return void
     */
    public function show()
    {
        // Check for Activation banner class.
        if ( ! class_exists('Give_Addon_Activation_Banner')) {
            include GIVE_PLUGIN_DIR . 'includes/admin/class-addon-activation-banner.php';
        }

        // Only runs on admin.
        $args = [
            'file' => GIVE_INVESTMENT_FILE,
            'name' => GIVE_INVESTMENT_NAME,
            'version' => GIVE_INVESTMENT_VERSION,
            'settings_url' => admin_url('edit.php?post_type=give_forms&page=give-settings&tab=give-investment'),
            'documentation_url' => 'https://gitlab.com/future-fund/frontend-dashboard/',
            'support_url' => 'https://giveforgood.world/',
            'testing' => false, // Never leave true.
        ];

        new Give_Addon_Activation_Banner($args);
    }
}
