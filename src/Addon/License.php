<?php

namespace GiveInvestment\Addon;

use Give_License;

class License
{

    /**
     * Check add-on license.
     *
     * @since 1.0.0
     * @return void
     */
    public function check()
    {
        new Give_License(
            GIVE_INVESTMENT_FILE,
            GIVE_INVESTMENT_NAME,
            GIVE_INVESTMENT_VERSION,
            'GiveWP'
        );
    }
}
