SELECT
    LatestExport.`Donation_total`,
    LatestExport.`Fund_description`,
    DonationMeta.meta_value as 'Campaign',
    DonationPerformance.Worth_Amount as 'Donation Worth',
    DonationPerformance.Transferred_Amount as 'Transferred Amount'
FROM
    ff_export as LatestExport
join
    wp_give_donors as Donor on Donor.id = LatestExport.Donor_id
LEFT JOIN
    wp_give_donationmeta AS DonationMeta
        ON (
            DonationMeta.donation_id = LatestExport.donation_id AND
            DonationMeta.meta_key = '_give_payment_form_title'
        )
LEFT JOIN
    wp_donation_performance_last AS DonationPerformance
        ON DonationPerformance.Donation_id = LatestExport.donation_id
WHERE
    Donor.user_id = %CURRENT_USER_ID%