SELECT
    SUM(DonationPerformance.Exchanged_Amount) as Initial,
    SUM(DonationPerformance.Worth_Amount) - SUM(DonationPerformance.Transferred_Amount) as Current,
    SUM(DonationPerformance.Transferred_Amount) as Transferred,
    DonationPerformance.Create_DateTime as DateTime
FROM
    wp_donation_performance as DonationPerformance
WHERE
    DonationPerformance.`Donor_id` = %CURRENT_USER_ID%
GROUP BY
    DonationPerformance.Create_DateTime