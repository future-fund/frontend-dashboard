up: build upload

upload:
    scp givewp-addon-investment.zip deploy@140.82.59.251:~/public_html/wp-content/plugins/givewp-addon-investment
    ssh deploy@140.82.59.251 \
        'cd ~/public_html/wp-content/plugins/givewp-addon-investment && \
        find . ! -name "givewp-addon-investment.zip" -type f -exec rm -f {} + && \
        unzip givewp-addon-investment.zip' && \
        rm givewp-addon-investment.zip

build:
    mkdir -p .cache/workdir
    podman run -v .:/workdir/output $(podman build -q .)

clean:
    podman container cleanup --rm --all
    podman image prune --all --force
