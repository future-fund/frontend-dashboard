FROM php:7.4-cli

# Install curl
RUN apt-get update && \
    apt-get install -y curl zip unzip

# Install composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install npm
ENV NODE_VERSION=16.16.0
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash
ENV NVM_DIR=/root/.nvm
RUN . "$NVM_DIR/nvm.sh" && nvm install ${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm use v${NODE_VERSION}
RUN . "$NVM_DIR/nvm.sh" && nvm alias default v${NODE_VERSION}
ENV PATH="/root/.nvm/versions/node/v${NODE_VERSION}/bin/:${PATH}"

WORKDIR /workdir

# Copy files
COPY . .

# Build and package
CMD composer install && \
    composer update && \
    npm install && \
    npm run dev && \
    ls -lhta && \
    zip -r givewp-addon-investment.zip LICENSE give-investment.php languages public readme.txt src uninstall.php vendor && \
    cp givewp-addon-investment.zip output
