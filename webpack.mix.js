const mix = require( 'laravel-mix' );
const wpPot = require( 'wp-pot' );

mix
	.setPublicPath( 'public' )
	.sourceMaps( false )

	// public assets
	.js( 'src/Investment/resources/js/frontend/give-investment.js', 'public/js/' )
	.sass( 'src/Investment/resources/css/frontend/give-investment-frontend.scss', 'public/css' )

	// images
	.copy( 'src/Investment/resources/images/*.{jpg,jpeg,png,gif}', 'public/images' );

mix.webpackConfig( {
	externals: {
		$: 'jQuery',
		jquery: 'jQuery',
	},
} );

if ( mix.inProduction() ) {
	wpPot( {
		package: 'Give - Investment',
		domain: 'give-investment',
		destFile: 'languages/give-investment.pot',
		relativeTo: './',
		bugReport: 'https://github.com/impress-org/give-investment/issues/new',
		team: 'Give for Good',
	} );
}
